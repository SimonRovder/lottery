import matplotlib.pyplot as plt
from itertools import groupby
import json
from lottery import fetch_data


users = fetch_data()
N = len(users)
statistics = dict()
for user in users:
    statistics[user[1]] = statistics.get(user[1], 0) + 1


with open('winners.json', 'r') as f:
    data = json.loads(f.read())
    top_500, top_2000, remainder = data['top_500'], data['top_2000'], data['remainder']


fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2, ncols=2)

ax0.plot(range(21), [statistics.get(i, 0) for i in range(21)])
ax0.set_xlabel('Ticket Bracket')
ax0.set_ylabel('Users')
ax0.set_title("Users by Ticket Bracket")

for dataset, label in zip((top_500, top_2000, remainder), ("Top 500 Draw", "Top 2000 Draw", "Others")):
    xs = range(21)
    ys = [0 for _ in xs]
    dataset.sort(key=lambda x: x[1])
    for tickets, users in groupby(dataset, lambda x: x[1]):
        ys[tickets] = float(len(list(users)))
    ax1.plot(xs, ys, label=label)
ax1.set_title('Users by Ticket Bracket')
ax0.set_xlabel('Ticket Bracket')
ax0.set_ylabel('Users')
ax1.legend()

for dataset, label in zip((top_500, top_2000, remainder), ("Top 500 Draw", "Top 2000 Draw", "Others")):
    xs = range(21)
    ys = [0 for _ in xs]
    dataset.sort(key=lambda x: x[1])
    pg = float(len(dataset)) / N
    for tickets, users in groupby(dataset, lambda x: x[1]):
        ptg = float(len(list(users))) / len(dataset)
        pt = float(statistics.get(tickets, 0)) / N
        # ys[tickets] = count / len(dataset) / statistics[tickets]
        ys[tickets] = ptg*pg/pt
    ax2.plot(xs, ys, label=label)
ax2.set_title('Probability of Getting Drawn by Ticket Bracket')
ax2.set_xlabel('Ticket Bracket')
ax2.set_ylabel('Probability')
ax2.legend()

for dataset, label in zip((top_500, top_2000, remainder), ("Top 500 Draw", "Top 2000 Draw", "Others")):
    xs = range(21)
    ys = [0 for _ in xs]
    dataset.sort(key=lambda x: x[1])
    for tickets, users in groupby(dataset, lambda x: x[1]):
        ys[tickets] = float(len(list(users))) / len(dataset) / statistics[tickets]
    ax3.plot(xs, ys, label=label)
ax3.set_title('Ratio of Users per Group by Ticket Bracket\n(Weighted by Ticket Bracket Frequency)')
ax3.legend()
ax3.set_xlabel('Ticket Bracket')
ax3.set_ylabel('Ratio')
plt.show()
